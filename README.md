#include <iostream>

// Define the Node class
class Node {
public:
    int data;
    Node* next;

    Node() {
        data = 0;
        next = nullptr;
    }

    Node(int data) {
        this->data = data;
        this->next = nullptr;
    }
};

// Define the Linkedlist class
class Linkedlist {
    Node* head;

public:
    Linkedlist() {
        head = nullptr;
    }

    // Function to insert a node at the end of the list
    void insertNode(int data) {
        Node* newNode = new Node(data);
        if (head == nullptr) {
            head = newNode;
            return;
        }
        Node* temp = head;
        while (temp->next != nullptr) {
            temp = temp->next;
        }
        temp->next = newNode;
    }

    // Function to print the linked list
    void printList() {
        Node* temp = head;
        if (head == nullptr) {
            std::cout << "List is empty." << std::endl;
            return;
        }
        while (temp != nullptr) {
            std::cout << temp->data << " ";
            temp = temp->next;
        }
    }

    // Function to delete a node by index
    void deleteNode(int nodeOffset) {
        Node* temp1 = head;
        Node* temp2 = nullptr;
        int listLen = 0;

        while (temp1 != nullptr) {
            temp1 = temp1->next;
            listLen++;
        }

        if (listLen < nodeOffset) {
            std::cout << "Index out of range." << std::endl;
            return;
        }

        temp1 = head;
        if (nodeOffset == 1) {
            head = head->next;
            delete temp1;
            return;
        }

        while (nodeOffset-- > 1) {
            temp2 = temp1;
            temp1 = temp1->next;
        }

        temp2->next = temp1->next;
        delete temp1;
    }
};

int main() {
    Linkedlist list;
    list.insertNode(1);
    list.insertNode(2);
    list.insertNode(3);
    list.insertNode(4);

    std::cout << "Elements of the list are: ";
    list.printList();
    std::cout << std::endl;

    list.deleteNode(2);

    std::cout << "Elements of the list after deletion: ";
    list.printList();
    std::cout << std::endl;

    return 0;
}
